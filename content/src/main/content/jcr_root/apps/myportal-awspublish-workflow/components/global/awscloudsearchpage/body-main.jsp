<%@include file="/libs/foundation/global.jsp"%>
<%@ page import="java.text.SimpleDateFormat,java.util.Calendar,java.util.Date,java.util.GregorianCalendar,com.wyn.myportal.brandstandards.service.AWSSearchService"%>
<%@page import="com.wyn.myportal.util.*"%>

<%
    AWSSearchService searchService = sling.getService(com.wyn.myportal.brandstandards.service.AWSSearchService.class);
%>
	<c:set var="pagetitle" value="<%=properties.get("jcr:title")%>" />
	<!-- Main Content Starts -->
	<div class="container-fluid">

		<div class="row">
			<div class="col-md-12">
				<div class="page-header">
					<h1>AWS CloudSearch Page</h1>
				</div>
			</div>
		</div>

		<div class="row">
                <cq:include path="par" resourceType="/apps/myportal/components/content/parsys" />
            	From Service: <% searchService.purgeAllData(false); %>
			</div>
		</div>

	</div>
	<!-- Main Content Ends -->

