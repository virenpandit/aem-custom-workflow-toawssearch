<%
%><%@ page
    import="com.day.cq.tagging.Tag,
                   com.day.cq.tagging.TagManager,
                   java.util.Iterator,
                   java.util.TreeMap,
                   java.util.Map,
                   java.util.Set"%>

<%
%><%@ include file="/libs/foundation/global.jsp" %>
<%
    response.setContentType("text/plain");
    String tags="";
    String tagPath = request.getParameter("tagpath");
    Resource tagResource = resource.getResourceResolver().getResource(tagPath);
    Tag tag = tagResource.adaptTo(Tag.class);
 %>
[<%

    String delim = "";
    Iterator<Tag> iter = tag.listChildren();
    Map<String, String> map = new TreeMap<String, String>();
   
    while (iter.hasNext()) {
        Tag childTag = iter.next();
        map.put(childTag.getTitle(),request.getParameter("tagpath")+"/"+childTag.getName());
    }
    Set entries = map.entrySet(); 
    for (Iterator iterator = entries.iterator(); iterator.hasNext();) { 
        Map.Entry entry = (Map.Entry) iterator.next(); 
        %><%= delim %>
        <%
               %>{<%
                   %>"text":"<%= entry.getKey() %>",<%
                   %>"value":"<%= entry.getValue() %>"<%
               %>}<%
               if ("".equals(delim)) delim = ",";
     }
%>]
           