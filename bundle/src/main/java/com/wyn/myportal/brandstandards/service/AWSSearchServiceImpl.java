package com.wyn.myportal.brandstandards.service;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.wyn.myportal.brandstandards.service.AWSOSGiConfiguration;
import com.wyn.myportal.util.MyPortalConstant;
import com.wyn.myportal.util.MyPortalUtil;
import org.apache.commons.lang3.StringUtils;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;

import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.jcr.api.SlingRepository;
import org.osgi.framework.Constants;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import aws.services.cloudsearchv2.AmazonCloudSearchClient;
import aws.services.cloudsearchv2.documents.AmazonCloudSearchAddRequest;
import aws.services.cloudsearchv2.documents.AmazonCloudSearchDeleteRequest;
import aws.services.cloudsearchv2.search.AmazonCloudSearchQuery;
import aws.services.cloudsearchv2.search.AmazonCloudSearchResult;
import aws.services.cloudsearchv2.search.Hit;
import com.amazonaws.auth.BasicAWSCredentials;

import javax.jcr.Node;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.xml.bind.DatatypeConverter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service(value = AWSSearchService.class)
@Component(metatype = false)
public class AWSSearchServiceImpl implements AWSSearchService {

    //private Session session;

    @Reference
    private SlingRepository repository;
    @Reference
    private QueryBuilder queryBuilder;
    @Reference
    private ResourceResolverFactory resolverFactory;
    @Reference
    private AWSOSGiConfiguration osgiConfigService;

    private AmazonCloudSearchClient awsClient;

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    public AmazonCloudSearchAddRequest getAWSSearchRecord(Node node, String aem_host) {

        log.debug("Entering: getAWSSearchRecord().v2");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = null;
        Date date = null;
        String stringDate = null;

        try {
            ResourceResolver resourceResolver = getResourceResolverForRead();
            Resource pageResource = resourceResolver.getResource(node.getPath());
            ValueMap valueMap = pageResource.adaptTo(ValueMap.class);
                                                            
            javax.jcr.PropertyIterator iterator = node.getProperties();
            AmazonCloudSearchAddRequest doc = new AmazonCloudSearchAddRequest();

            if (iterator.hasNext()) {
                log.debug("Iterating through node attributes");
                while (iterator.hasNext()) {
                    javax.jcr.Property prop = (javax.jcr.Property) iterator.nextProperty();
                    String propertyName = StringUtils.defaultIfBlank(prop.getName(), "");

                    if(!StringUtils.isBlank(propertyName)) {
                        if ("jcr:uuid".equalsIgnoreCase(propertyName)) {
                            addFieldToAWSRequest(prop, doc, "id", resourceResolver);

                            String jcrUUID = (String) prop.getValue().getString();
                            log.debug("Adding ID: " + jcrUUID + " for Property: " + propertyName);
                            doc.id = jcrUUID;

                        } else if ("jcr:title".equalsIgnoreCase(propertyName)) {
                            addFieldToAWSRequest(prop, doc, "headline", resourceResolver);
                        } else if ("onTime".equalsIgnoreCase(propertyName)) {
                            date = prop.getValue().getDate().getTime();
                            stringDate = MyPortalUtil.findAPFormattedDate(date);
                            String publishDate =  MyPortalUtil.customDateFormatter(date, MyPortalConstant.SOLR_DATEFORMATZ);
                            if (publishDate != null) {
                                doc.addField("publish_dt", publishDate);
                            }
                        } else if ("offTime".equalsIgnoreCase(propertyName)) {
                            date = prop.getValue().getDate().getTime();
                            stringDate = MyPortalUtil.findAPFormattedDate(date);
                            String expiryDate =  MyPortalUtil.customDateFormatter(date, MyPortalConstant.SOLR_DATEFORMATZ);
                            if (expiryDate != null) {
                                doc.addField("expiry_dt", expiryDate);
                            }
                        } else if ("categories".equalsIgnoreCase(propertyName)) {
                            addFieldToAWSRequest(prop, doc, "category", resourceResolver);
                        } else if ("coverage".equalsIgnoreCase(propertyName)) {
                            addFieldToAWSRequest(prop, doc, "coverage", resourceResolver);
                        } else if ("sections".equalsIgnoreCase(propertyName)) {
                            addFieldToAWSRequest(prop, doc, "section", resourceResolver);
                        } else if ("region".equalsIgnoreCase(propertyName)) {
                            addFieldToAWSRequest(prop, doc, "region", resourceResolver);
                        } else if ("brand".equalsIgnoreCase(propertyName)) {
                            addFieldToAWSRequest(prop, doc, "brand", resourceResolver);
                        }
                    }
                }
            }

            // doc.version = 1;

            //Add pagename for the search
            log.debug("Adding pagename: " + node.getParent().getName());
            doc.addField("pagename", node.getParent().getName());

            StringBuilder fulltext = getNodeTextData(node.getPath());
            if (!StringUtils.isBlank(fulltext.toString())) {
                log.debug("Adding content_text: " + fulltext.toString());
                doc.addField("content_text", fulltext.toString());
            }
            log.debug("Node Path =" + node.getPath());
            // jsonRecord.put("url", AEM_HOST + node.getPath());

            log.debug("Adding uri: " + aem_host + node.getParent().getPath() + ".html");
            doc.addField("uri", aem_host + node.getParent().getPath() + ".html");

            log.debug("Exit: getAWSSearchRecord().v2, doc=" + doc.toString());
            log.debug(doc.toString());
            return doc;
        } catch (Exception e){
            e.printStackTrace();
            log.error("Error in AWS CloudSearch" + e.getMessage());
        }
        log.debug("Exiting: getAWSSearchRecord() with NULL");
        return null;
    }

    //public void updateSolrIndex(SolrInputDocument solrInputFields, String zkUrl, String zkDefaultCollection) {
    public void updateAWSIndex(AmazonCloudSearchAddRequest doc) {

        log.debug("Entering: updateAWSIndex.v4()");

        try {
            if(null==awsClient)
                initializeAWSClient();

            log.debug("Adding Document to AWS CloudSearch");
            awsClient.addDocument(doc);
            log.debug("AWS CloudSearch updated successfully");
            
            log.debug("Exiting updateAWSIndex()");
        }
        catch (Exception e){
            log.error("Error in updateAWSIndex() ", e);
        }
    }

    public List query(String q) {
        return advancedQuery(q, "lucene", 0, 2000, "or");
    }

    public List advancedQuery(String q, String parser, int start, int totalResults, String op) {

        log.debug("Entering: queryAWSSearch(query: " + q + ", start=" + start + ", totalResults=" + totalResults + 
                                            ", op=" + op + ")");

        Session session = null;

        ArrayList<Node> resultList = new ArrayList<Node>();
        try {
            if(null==this.awsClient)
                initializeAWSClient();

            session = getSessionForRead();

            AmazonCloudSearchQuery query = new AmazonCloudSearchQuery();
            query.query = q;
            query.queryParser = StringUtils.defaultIfBlank(parser, "lucene");
            query.start = start;
            query.size = totalResults;
            query.setDefaultOperator(StringUtils.defaultIfBlank(op, "or"));

            log.debug("Querying Document from AWS CloudSearch");
            AmazonCloudSearchResult result = awsClient.search(query);
            log.debug("Query Complete! Total hits: " + result.found);
            for(Hit hit: result.hits) {
                log.debug("Record.id: " + hit.id);
                String nodePath = StringUtils.chomp(hit.getField("uri"), ".html");
                if(session.nodeExists(nodePath)) {
                    log.debug("Adding to list, node: " + nodePath);
                    Node node = session.getNode(nodePath); // + "/jcr:content");
                    resultList.add(node);
                } else {
                    log.warn("No matching node for AWS Search record's uri: " + nodePath);
                }
            }

            log.debug("Exiting queryAWSSearch(total-results-in-list: " + resultList.size() + ")");
        } catch (Exception e){
            log.error("Error in queryAWSSearch() ", e);
        } finally {
            try {
                if(null!=session)
                    session.logout();
            } catch(Exception iex) { }
        }
        
        return resultList;
    }
    
    public void purgeAllData(boolean dryRun) {
        
        log.debug("Inside purgeAllData(dryrun=" + dryRun + ")");
        try {
            if(null==this.awsClient)
                initializeAWSClient();

            AmazonCloudSearchQuery query = new AmazonCloudSearchQuery();
            query.queryParser = "lucene";
            query.query = "*";
            query.start = 0;
            query.size = 10000;
            query.setDefaultOperator("or");

            log.debug("Querying Document from AWS CloudSearch");
            AmazonCloudSearchResult result = this.awsClient.search(query);
            log.debug("Total documents in collection: " + result.found);
            for(Hit hit: result.hits) {
                if(dryRun) {
                    log.debug("Could delete record with id: " + hit.id);
                } else {
                    log.debug("Deleting Document from AWS CloudSearch: id: " + hit.id);
                    AmazonCloudSearchDeleteRequest deleteRequest = new AmazonCloudSearchDeleteRequest();
                    deleteRequest.id = hit.id;
                    deleteRequest.version = 1;
                    awsClient.deleteDocument(deleteRequest);
                }
            }
            log.debug("Exiting purgeAllData()");
        } catch (Exception e){
            log.error("Error in purgeAllData() ", e);
        }    
    }

    public StringBuilder getNodeTextData(String path) throws Exception {
        log.debug("Entering : getNodeTextData(path=" + path + ")");
        
        Session session = null;
        StringBuilder builder = new StringBuilder();
        try {
            session = getSessionForRead();

            Map<String, String> map = new HashMap<String, String>();
            log.debug("PATH: " + path);
            map.put("path", path);
            map.put("property", "text");
            map.put("property.operation", "exists");
            Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
            query.setStart(0);
            SearchResult result = query.getResult();
            builder = new StringBuilder();
            Iterator<Node> nodeItr = result.getNodes();
            while (nodeItr.hasNext()) {
                Node node = nodeItr.next();
                if (node.hasProperty("text")) {
                    builder.append(node.getProperty("text").getValue().getString());
                }
            }
        } catch (Exception ex) {
            log.error("Exception occurred in getNodeTextData()", ex);
        } finally {
            if (null != session) {
                session.logout();
            }
        }

        log.debug("Exit: getNodeTextData()");
        return builder;
    }

    private void addFieldToAWSRequest(javax.jcr.Property prop, AmazonCloudSearchAddRequest doc, String searchFieldName, 
                                            ResourceResolver resourceResolver) throws Exception {

        log.debug("Inside addFieldToAWSRequest() attribute-name=" + prop.getName());
        ArrayList<String> list = new ArrayList<String>();
        if(prop.isMultiple()) {
            Value[] value = prop.getValues();
            log.debug("Adding attribute[]: " + searchFieldName + "=" + value);

            if (!StringUtils.isBlank(searchFieldName) && value.length > 0) {
                for (int i=0; i<value.length; i++) {
                    // list.add(MyPortalUtil.getTagTitle(value[i].getString(), resourceResolver, ""));
                    String tag = value[i].getString();
                    if(StringUtils.contains(tag, '/'))
                        tag = StringUtils.substringAfterLast(tag, "/");
                    if(StringUtils.isNotBlank(tag)) {
                        list.add(tag);
                    }
                }
                if(list.size()>0)
                    doc.addField(searchFieldName, list);
            }
        } else{
            // String value = (String) prop.getValue().getString();
            // value = MyPortalUtil.getTagTitle(value, resourceResolver, "");
            String tag = (String) prop.getValue().getString();
            if(StringUtils.contains(tag, '/'))
                tag = StringUtils.substringAfterLast(tag, "/");
            if (StringUtils.isNotBlank(searchFieldName) && StringUtils.isNotBlank(tag)) {
                log.debug("Adding attribute: " + searchFieldName + "=" + tag);
                doc.addField(searchFieldName, tag);
            }
        }
        log.debug("Exiting addFieldToAWSRequest()");
    }
    
    private void initializeAWSClient() {
        String accessKey = osgiConfigService.getProperty("AWS_ACCESS");
        String secretKey = osgiConfigService.getProperty("AWS_SECRET");
        String docEndpoint = osgiConfigService.getProperty("AWS_CLOUDSEARCH_DOC_ENDPOINT");
        String searchEndpoint = osgiConfigService.getProperty("AWS_CLOUDSEARCH_SEARCH_ENDPOINT");

        AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
        this.awsClient = new AmazonCloudSearchClient(credentials);
        this.awsClient.setSearchEndpoint(searchEndpoint);
        this.awsClient.setDocumentEndpoint(docEndpoint);
    }

    private ResourceResolver getResourceResolverForRead() throws Exception {
        Map<String, Object> param = new HashMap<String, Object>();
        param.put(ResourceResolverFactory.SUBSERVICE, "readService");
        //ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
        ResourceResolver resourceResolver = resolverFactory.getServiceResourceResolver(param);
        return resourceResolver;
    }

    private Session getSessionForRead() throws Exception {
        return getResourceResolverForRead().adaptTo(Session.class);
    }

    public String test(String op) {
        return "Hello " + op;
    }
}
