package com.wyn.aemquery.workflow;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.search.result.Hit;
import com.day.cq.wcm.api.Page;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;

import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.Value;
import java.text.SimpleDateFormat;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import com.wyn.myportal.brandstandards.service.AWSOSGiConfiguration;
import com.wyn.myportal.brandstandards.service.AWSSearchService;
import aws.services.cloudsearchv2.documents.AmazonCloudSearchAddRequest;

/**
 * Workflow Step for querying and bulk publish of AEM pages to AWS CloudSearch
 */
@Service
@Properties({
		@Property(name = Constants.SERVICE_DESCRIPTION, value = "AEM Bulk Publish to AWS CloudSearch"),
		@Property(name = Constants.SERVICE_VENDOR, value = "Wyndham"),
		@Property(name = "process.label", value = "Brand Standards: Bulk Publish to AWS CloudSearch") })
@Component(metatype = false)
public class AEMBulkPublishWorkflowProcessStep implements WorkflowProcess {

	private Session session;
	@Reference
	private SlingRepository repository;
	@Reference
	private QueryBuilder queryBuilder;
	@Reference
	private ResourceResolverFactory resolverFactory;
	@Reference
	private AWSOSGiConfiguration osgiConfigService;
    @Reference
    private AWSSearchService awsSearchService;

	/** Default log. */
	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	public String getRepositoryName() {
		return repository.getDescriptor(Repository.REP_NAME_DESC);
	}

	public void execute(WorkItem workItem, WorkflowSession wfSession, MetaDataMap metadata) throws WorkflowException {

		try {
			log.debug("Entering execute()");

			ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			Session session = resourceResolver.adaptTo(Session.class);
			Node node = session.getNode(workItem.getWorkflowData().getPayload().toString());

            String q = "/jcr:root" + workItem.getWorkflowData().getPayload().toString() + "//*[@jcr:primaryType='cq:Page']";
            log.debug("Querying Node from Parent-Path: " + workItem.getWorkflowData().getPayload().toString());
            traverseJcrAndPublishToAWS(workItem.getWorkflowData().getPayload().toString());

			log.debug("Exit: execute()");
		} catch (Exception ex) {
            log.error("Error in : AEMBulkPublishWorkflowProcessStep execute()");
			throw new WorkflowException(ex);
		}
	}

    public StringBuilder traverseJcrAndPublishToAWS(String path) throws Exception {
        log.debug("Entering : traverseJcrAndPublishToAWS(path=" + path + ")");

        StringBuilder builder = new StringBuilder();

        try {
            ResourceResolver resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
            session = resourceResolver.adaptTo(Session.class);
            Map<String, String> map = new HashMap<String, String>();
            map.put("path", path);
            map.put("type", "cq:Page");
            map.put("p.limit", "2000");
            Query query = queryBuilder.createQuery(PredicateGroup.create(map), session);
            query.setStart(0);
            SearchResult result = query.getResult();
            int documentsUploaded = 0;
            for(Hit hit: result.getHits()) {
                log.debug("Node-Name: " + hit.getNode());
                Node node = session.getNode(hit.getPath()+ "/jcr:content");
                AmazonCloudSearchAddRequest doc =  awsSearchService.getAWSSearchRecord(node, "");
                if( null != doc){
                    //Update AWS CloudSearch Engine
                   awsSearchService.updateAWSIndex(doc);
                   documentsUploaded++;
                }
            }
            log.debug("Total page uploaded to AWS CloudSearch: " + documentsUploaded);
        } catch (Exception ex) {
            log.error("Exception occurred in deleteFromSolrSearch:" + ex);
            ex.printStackTrace();
        } finally {
            if (null != session) {
                session.logout();
            }
        }

        log.debug("Exit: traverseJcrAndPublishToAWS()");
        return builder;
    }
}
