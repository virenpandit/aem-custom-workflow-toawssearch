package com.wyn.myportal.brandstandards.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.SlingServletException;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.ComponentContext;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Dictionary;
import javax.xml.bind.DatatypeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* Implementation of AWSOSGiConfiguration provides OsgiConfig parameters.
*/
@Service(value={
        AWSOSGiConfiguration.class,
        javax.servlet.Servlet.class
 })

@Component(immediate = true, metatype = true)
@Properties({
        @Property(name="AEM_HOST", label="AEM Host", value="http://localhost:4502"),
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "Brand Standards Service Configuration"),
        @Property(name = Constants.SERVICE_VENDOR, value = "Wyndham"),
        @Property(name = "process.label", value = "Brand Standards - AWS CloudSearch - Configuration Service"),
        @Property(name = "ZK_URL", label="Zookeeper Url", value = "http://solr.com:9095"),
        @Property(name = "ZK_DEFAULT_COLLECTION", label="Zookeeper Default Collection", value = "brandstandards"),
        @Property(name = "AWS_ACCESS", label="AWS Access Key", value = "AWSACCESSKEY"),
        @Property(name = "AWS_SECRET", label="AWS Secret Key", value = "AWSSECRET"),
        @Property(name = "AWS_CLOUDSEARCH_DOC_ENDPOINT", label="AWS CloudSearch Document Endpoint", value = "doc-endpoint.cloudsearch.amazonaws.com"),
        @Property(name = "AWS_CLOUDSEARCH_SEARCH_ENDPOINT", label="AWS CloudSearch Search Endpoint", value = "search-endpoint.us-west-2.cloudsearch.amazonaws.com"),
		@Property(name="sling.servlet.paths", value = "/bin/config"),
        @Property(name="sling.servlet.selectors",value="brandstandards"),
        @Property(name="sling.servlet.extensions",value="json")
})

public class AWSOSGiConfiguration extends SlingAllMethodsServlet {

	private Dictionary<String, String> properties;
	protected final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("unchecked")
	    protected void activate(ComponentContext context) {
	        properties = context.getProperties();
	        //Check the properties are reading from OsgiConfig
	        /*for (Enumeration<String> e = properties.keys(); e.hasMoreElements();) {
	        	       log.info("Key value from OSGI Config : " + (String) e.nextElement());
	        }*/
	    }

	    protected void deactivate(ComponentContext context) {
	        properties = null;
	    }

	    public String getProperty(String key) {
	    	String config_key = null;
	    	if(null != properties){
	    		config_key = properties.get(key);
                if(key.equalsIgnoreCase("AWS_ACCESS") || key.equalsIgnoreCase("AWS_SECRET")) {
                    config_key = getA(config_key);
                }
	    		log.info("Osgi config value of " + key + " is: "+ config_key);
	    	}
	        return config_key;     
	    } 

	    public void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws SlingServletException, IOException {
			PrintWriter out = response.getWriter();
			String property = request.getParameter("param");
			String value = "";
			if(StringUtils.isNotBlank(property)){
				value = getProperty(property);
                if(property.equalsIgnoreCase("AWS_ACCESS") || property.equalsIgnoreCase("AWS_SECRET")) {
                    value = getA(value);
                }
			}
			out.write(value);

		}

    private String getA(String _A) {
        return new String(DatatypeConverter.parseBase64Binary(_A));
    }        
}
