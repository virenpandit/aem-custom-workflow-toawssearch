package com.wyn.myportal.brandstandards.workflow;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.SearchResult;
import com.day.cq.wcm.api.Page;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowSession;
import com.day.cq.workflow.exec.WorkItem;
import com.day.cq.workflow.exec.WorkflowProcess;
import com.day.cq.workflow.metadata.MetaDataMap;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.lang3.StringUtils;

import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Reference;

import org.apache.felix.scr.annotations.Properties;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONObject;
import org.apache.sling.jcr.api.SlingRepository;

import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.Session;
import javax.jcr.Value;
import java.text.SimpleDateFormat;
// import java.util.*;

import com.wyn.myportal.brandstandards.service.AWSSearchService;
import aws.services.cloudsearchv2.documents.AmazonCloudSearchAddRequest;

import com.wyn.myportal.util.MyPortalUtil;
import com.wyn.myportal.brandstandards.service.AWSOSGiConfiguration;


/**
 * Workflow Step for posting AEM content to AWS CloudSearch. Note that the repository is injected, not
 * retrieved.
 */
@Service
@Properties({
		@Property(name = Constants.SERVICE_DESCRIPTION, value = "Brand Standards Publish to AWS CloudSearch Workflow process implementation"),
		@Property(name = Constants.SERVICE_VENDOR, value = "Wyndham"),
		@Property(name = "process.label", value = "Brand Standards: Publish to AWS CloudSearch") })
@Component(metatype = false)
public class AWSPublishPageWorkflowProcessStep implements WorkflowProcess {

	private Session session;
	@Reference
	private SlingRepository repository;
	@Reference
	private QueryBuilder queryBuilder;
	@Reference
	private ResourceResolverFactory resolverFactory;
	@Reference
	private AWSOSGiConfiguration osgiConfigService;

    @Reference
    private AWSSearchService awsSearchService;

	/** Default log. */
	protected final Logger log = LoggerFactory.getLogger(this.getClass());

	public String getRepositoryName() {
		return repository.getDescriptor(Repository.REP_NAME_DESC);
	}

	public void execute(WorkItem workItem, WorkflowSession wfSession, MetaDataMap metadata) throws WorkflowException {

        ResourceResolver resourceResolver = null;
        Session session = null;
		try {
			log.debug("Entering execute()");

			resourceResolver = resolverFactory.getAdministrativeResourceResolver(null);
			session = resourceResolver.adaptTo(Session.class);
			Node node = session.getNode(workItem.getWorkflowData().getPayload().toString()+ "/jcr:content");
            AmazonCloudSearchAddRequest doc =  awsSearchService.getAWSSearchRecord(node, "");
            if( null != doc){
                //Update AWS CloudSearch Engine
               awsSearchService.updateAWSIndex(doc);
            }
			log.debug("Exit: execute()");
		} catch (Exception ex) {
            log.error("Error in : AWSPublishPageWorkflowProcessStep execute()", ex);
			throw new WorkflowException(ex);
		} finally {
            try {
                if(null!=session)
                    session.logout();
            } catch(Exception iex) { }
        }
	}
}
