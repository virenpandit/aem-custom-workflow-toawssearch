package com.wyn.myportal.brandstandards.service;

import javax.jcr.Node;
import aws.services.cloudsearchv2.documents.AmazonCloudSearchAddRequest;
import java.util.List;

public interface AWSSearchService {

    public AmazonCloudSearchAddRequest getAWSSearchRecord(Node node, String aem_host);
    public void updateAWSIndex(AmazonCloudSearchAddRequest awsAddRequest);
    public List query(String q);
    public List advancedQuery(String q, String parser, int start, int totalResults, String op);
    public void purgeAllData(boolean dryRun);
    
    public String test(String op);
}
