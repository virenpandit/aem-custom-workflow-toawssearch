package com.wyn.myportal.util;

/**
 * Created by C13774 on 8/10/2014.
 */
public class MyPortalConstant {

    /*
	 * namespaces for page tags
	 */
    public static final String TAG_BASEPATH    = "/etc/tags/";
    public static final String BRAND_TAG        = "brand";
    public static final String CATEGORY_TAG		= "brandmatterscategory";
    public static final String LANGUAGE_TAG     = "language";
    public static final String REGION_TAG       = "region";
    public static final String ROLE_TAG         = "role";
    public static final String SITESTATUS_TAG   = "sitestatus";
    public static final String TAG_NAMESPACE    = "myportal";
    public static final String CATEGORY_TEMPLATE    = "/apps/myportal/templates/categorypagetemplate";
    public static final String BRANDMATTERS_TEMPLATE    = "/apps/myportal/templates/brandmatterstemplate";
    public static final String VIDEOPAGE_TEMPLATE    = "/apps/myportal/templates/videopagetemplate";
    public static final String WCP_BRANDMATTERS    = "/Read%20Brand%20Matters%20";
    public static final String WCP_BRANDMATTERS_ARTICLE    = "/Brand%20Matters%20Detail";
    public static final String SEGMENT_TRAIT_RESOURCE_PATH =  "/jcr:content/traits/andpar";
    public static final String SOLR_DATEFORMAT = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String SOLR_DATEFORMATZ = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String SOHO_TEMPLATE    = "/apps/myportal/templates/sohotemplate";

}
