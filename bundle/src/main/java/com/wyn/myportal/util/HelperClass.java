package com.wyn.myportal.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class HelperClass {
	
	
	 /**
     * Formats the given input date into Associate Press Format.
     * 
      * <p>
     * Returns an empty<code>String</code> if null .
     * </p>
     * 
      * @param date
     * @return
     */
     public static String findAPFormattedDate(final Date date) {
                     Boolean flag = false;
                     final Calendar cal = new GregorianCalendar();
                     cal.setTime(date);
                     final String[] months = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
                     String formatedString = null;

                     try {

                                     final int year = cal.get(Calendar.YEAR);
                                     SimpleDateFormat monthParse = new SimpleDateFormat("MMM");
                                     String month = monthParse.format(date);
                                     final int currentDate = cal.get(Calendar.DATE);

                                     for (int i = 0; i < months.length; i++) {
                                                     if (month.equalsIgnoreCase(months[i])) {
                                                                     flag = true;
                                                                     break;
                                                     }
                                     }

                                     if (flag) {
                                                     //formatedString = month + ". " + currentDate + ", " + year;
                                                     formatedString = month + " " + currentDate + ", " + year;
                                     } else if (month.equalsIgnoreCase("sep")) {
                                                     month = "Sept";
                                                    // formatedString = month + ". " + currentDate + ", " + year;
                                                     formatedString = month + " " + currentDate + ", " + year;

                                     } else {
                                                     monthParse = new SimpleDateFormat("MMMM");
                                                     month = monthParse.format(date);
                                                     formatedString = month + " " + currentDate + ", " + year;
                                     }

                     } catch (Exception e) {
                                     //LOG.error("Exception in getDateAPFormated()!");
                     }

                     return formatedString;
     }

}
